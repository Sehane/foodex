import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/services/app/app.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  menuTags: any;
  menuTagParent = 'Salad';
  constructor(
    private appService: AppService,
  ) { }

  ngOnInit() {
    this.getList();
  }

  getList() {
    this.appService.getList({}, this.appService.RESTAURANT).subscribe(response => {
      this.menuTags = response.result.menu_tags;
    });
  }

  changeMenuTag(tag: any) {
    this.menuTagParent = tag;
  }

}
