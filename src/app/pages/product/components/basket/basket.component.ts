import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/pass-data.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {
  basketData;
  total = 0;
  constructor(private passDataService: DataService) { }

  @ViewChild('confirmModal', { static: false }) confirmModal: ElementRef;
  elm_confirm: HTMLElement;

  @ViewChild('successModal', { static: false }) successModal: ElementRef;
  elm_success: HTMLElement;

  ngAfterViewInit(): void {
    this.elm_confirm = this.confirmModal.nativeElement as HTMLElement;
    this.elm_success = this.successModal.nativeElement as HTMLElement;
  }

  ngOnInit() {
    this.passData();
  }

  passData() {
    this.passDataService.currentMessage.subscribe(data => {
      this.basketData = data;
    });

    this.passDataService.currentTotal.subscribe(data => {
      this.total = data;
    });
  }

  deleteItem(item: any) {
    this.basketData.filter(element => element.name == item.name)[0].count--;

    if (this.basketData.filter(element => element.name == item.name)[0].count == 0) {
      const indexElement = this.basketData.indexOf(this.basketData.filter(element => element.name == item.name)[0]);
      if (indexElement > -1) {
        this.basketData.splice(indexElement, 1);
      }
    }
    this.passDataService.changeMessage(this.basketData);
    this.calculateTotal(this.basketData);
  }

  calculateTotal(basketData) {
    this.total = 0;
    basketData.forEach(element => {
      this.total = this.total + (element.count * element.price);
    });
    this.passDataService.changeTotal(this.total);
  }

  close(element): void {
    element.classList.remove('show');
    setTimeout(() => {
    element.style.width = '0';
    }, 75);
  }

  open(element) {
    element.classList.add('show');
    element.style.width = '100vw';
  }

  openConfirm() {
    this.open(this.elm_confirm);
  }
  closeConfirm() {
    this.close(this.elm_confirm);
  }

  openSuccess() {
    this.checkOut();
    this.closeConfirm();
    this.open(this.elm_success);

  }
  closeSuccess() {
    this.close(this.elm_success);
  }

  checkOut() {
    this.basketData.forEach(element => {
      element.count = 0;
    });

    this.basketData = [];
    this.passDataService.changeMessage(this.basketData);
  }

}
