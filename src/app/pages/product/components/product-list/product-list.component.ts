import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Injectable, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { element } from 'protractor';
import { AppService } from 'src/app/services/app/app.service';
import { DataService } from 'src/app/services/pass-data.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
@Injectable({
  providedIn: 'root'
})
export class ProductListComponent implements OnInit {
  @Input() menuTagChild: string;

  response = [];
  localData = [];
  count: number;
  menu_items: any;

  constructor(
    private appService: AppService,
    private passDataService: DataService
  ) {
  }

  ngOnInit() {
    this.getList();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.getMenuItems();
  }

  getList() {
    this.appService.getList({}, this.appService.RESTAURANT).subscribe(response => {
      response.result.menus.forEach(element => {
        this.response = element.menu_sections;
        element.menu_sections?.forEach(menu_item => {
          menu_item.menu_items.forEach(menu => {
            menu['count'] = 0;
          });
        });
      });
      this.getMenuItems();
    });
  }

  getMenuItems() {
    this.menu_items = this.response.filter(element => element.section_name == this.menuTagChild)[0]?.menu_items;
  }

  filterList(array, name) {
    return array.filter(element => element.name == name)[0];
  }

  addToCart(item: any) {
    this.passDataService.currentMessage.subscribe(data => {
      this.localData = data;
    });

    this.menu_items.filter(element => element.name == item.name)[0].count++;

    if (this.filterList(this.localData, item.name)?.name == item.name) {
      this.passDataService.changeMessage(this.localData);
    }
    else {
      this.localData.push(item);
      this.passDataService.changeMessage(this.localData);
    }
    this.calculateTotal(this.localData);

  }

  deleteItem(item: any) {
    this.filterList(this.menu_items, item.name).count--;

    if (this.filterList(this.menu_items, item.name).count == 0) {
      const indexElement = this.localData.indexOf(this.filterList(this.localData, item.name));
      if (indexElement > -1) {
        this.localData.splice(indexElement, 1);
      }
    }
    this.passDataService.changeMessage(this.localData);

    this.calculateTotal(this.localData);
  }

  calculateTotal(total) {
    this.count = 0;
    total.forEach(element => {
      this.count = this.count + (element.count * element.price);
    });
    this.passDataService.changeTotal(this.count);
  }
}
