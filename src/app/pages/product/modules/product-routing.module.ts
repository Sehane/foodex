import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductComponent } from '../product.component';
import { ProductListComponent } from './../components/product-list/product-list.component';
import { BasketComponent } from '../components/basket/basket.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ProductComponent
      },
      {
        path: 'list',
        component: ProductListComponent
      },
      {
        path: 'basket',
        component: BasketComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
