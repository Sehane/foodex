import { NgModule } from '@angular/core';
import { SharedModule } from '../../../modules/shared.module';

import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from '../product.component';
import { ProductListComponent } from '../components/product-list/product-list.component';
import { BasketComponent } from '../components/basket/basket.component';

@NgModule({
    declarations: [
        ProductComponent,
        ProductListComponent,
        BasketComponent
    ],
    imports: [
        ProductRoutingModule,
        SharedModule
    ]
})
export class ProductsModule { }
