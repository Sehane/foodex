import { HttpsService } from './../https/https.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppService extends HttpsService {

  public RESTAURANT = 'restaurant/491068';

  constructor(public http: HttpClient) {
    super();
  }

  // GET methods

  public getList(params: any = {}, route_url): Observable<any> {
    return this.get(this.http, route_url, params);
  }

  public getItem(params: any = {}, route_url): Observable<any> {
    return this.get(this.http, `${route_url}/${params.id}`, params);
  }

  // POST/PUT methods

  public postdata(params: any = {}, route): Observable<any> {

    if (params.id) {
      return this.put(this.http, `${route}/${params.id}`, params);
    } else {
      return this.post(this.http, route, params);
    }
  }

  // DELETE Method

  public deleteData(params: any = {}, route): Observable<any> {
    return this.delete(this.http, `${route}/${params.id}`, params);
  }

}
