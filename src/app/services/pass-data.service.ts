import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private messageSource = new BehaviorSubject([]);
  private total = new BehaviorSubject(0);
  private menuTag = new BehaviorSubject('Salad');

  currentMessage = this.messageSource.asObservable();
  currentTotal = this.total.asObservable();
  currentMenuTag = this.menuTag.asObservable();

  constructor() { }

  changeMessage(message) {
    this.messageSource.next(message);
  }

  changeTotal(total: number) {
    this.total.next(total);
  }

  changeMenuTag(menuTag: string) {
    this.menuTag.next(menuTag);
  }
}