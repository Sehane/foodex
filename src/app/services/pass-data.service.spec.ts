/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PassDataService } from './pass-data.service';

describe('Service: PassData', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PassDataService]
    });
  });

  it('should ...', inject([PassDataService], (service: PassDataService) => {
    expect(service).toBeTruthy();
  }));
});
