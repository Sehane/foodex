import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const SharedComponents = [
    CommonModule
];

@NgModule({
    imports: [SharedComponents],
    exports: [SharedComponents]
})
export class SharedModule { }
