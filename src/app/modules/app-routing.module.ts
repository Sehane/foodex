import { ProductComponent } from '../pages/product/product.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../guard/auth.guard';


const routes: Routes = [
  {
    path: '',
    component: ProductComponent,
    canActivate: [AuthGuard],
    loadChildren: () => import('../pages/product/modules/product.module').then(m => m.ProductsModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
